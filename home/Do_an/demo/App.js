import React, {useState, useEffect} from 'react';
import {View, Text} from 'react-native';
// Link packet
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

// Link to Screen
import LoginScreen from './src/Screens/LoginScreen';
import AppScreen from './src/Screens/AppScreen';
import ForgetScreen from './src/Screens/ForgetScreen';

import MusicScreen from './src/Screens/MusicScreen';
import ProfileScreen from './src/Screens/ProfileScreen';
import SearchScreen from './src/Screens/SearchScreen';
import RegisterScreen from './src/Screens/RegisterScreen';

const Stack = createStackNavigator();

export default function MyStack() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="App">
        <Stack.Screen
          name="App"
          component={AppScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Register"
          component={RegisterScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Forget"
          component={ForgetScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Music"
          component={MusicScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Profile"
          component={ProfileScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Search"
          component={SearchScreen}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
