import React, {useState, useEffect} from 'react';
import {View, Text, ImageBackground, StyleSheet, Alert} from 'react-native';
import {Input, Button} from 'react-native-elements';

import Icon from 'react-native-vector-icons/Fontisto';

import {firebaseApp} from './../Components/firebaseConfig';

import firebase from 'react-native-firebase';

export default function ForgetScreen(props) {
  const [email, setEmail] = useState('');
  const onForget = () => {
    if (email !== '') {
      const actionCodeSettings = {
        url: 'https://www.music.com/?email=' + email,
        iOS: {
          bundleId: 'com.example.ios',
        },
        android: {
          packageName: 'com.demo.android',
          installApp: true,
          minimumVersion: '12',
        },
        handleCodeInApp: true,
        // When multiple custom dynamic link domains are defined, specify which
        // one to use.
        dynamicLinkDomain: 'https://rnlogin.page.link/4Yif',
      };
      const auth = firebase.auth();

      auth
        .sendPasswordResetEmail(email)
        .then(function() {
          // Email sent.
          Alert.alert(
            'Thông báo ',
            ' check ' + email,
            [
              {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              {
                text: 'OK',
                onPress: () => props.navigation.navigate('Login'),
              },
            ],
            {cancelable: false},
          );
        })
        .catch(function(error) {
          // An error happened.
          alert('email ' + email + ' khong hợp lệ !');
        });
      firebase
        .auth()
        .currentUser.sendEmailVerification(actionCodeSettings)
        .then(function() {
          // Verification email sent.
        })
        .catch(function(error) {
          // Error occurred. Inspect error.code.
        });
    } else {
      alert('Vui lòng nhập email');
    }
  };
  return (
    <ImageBackground
      source={require('./../../public/images/hinh-nen-dai-mau-tim-do-dep-cho-dien-thoai.jpg')}
      style={{width: '100%', height: '100%'}}>
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          // alignItems: 'center',
        }}>
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <Text style={styles.header}>Đặt lại mật khẩu</Text>
        </View>
        <View style={{marginHorizontal: 20}}>
          <Input
            placeholder="email "
            leftIcon={<Icon name="email" size={24} color="black" />}
            onChangeText={email => setEmail(email)}
            value={email}
            autoCompleteType={'email'}
          />
        </View>
        <View style={{justifyContent: 'space-around', flexDirection: 'row'}}>
          <View style={{marginTop: 10, width: 100}}>
            <Button title="Khôi phục" onPress={onForget} />
          </View>
        </View>
      </View>
    </ImageBackground>
  );
}
const styles = StyleSheet.create({
  header: {
    fontSize: 30,
    color: 'white',
  },
  warning: {
    fontSize: 20,
    color: 'orange',
  },
});
