import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  FlatList,
  ScrollView,
} from 'react-native';

import {firebaseApp, fireStore} from './firebaseConfig';

import MusicItem from '../Items/MusicItem';
import MusicItem2 from '../Items/MusicItem2';
// import PlayerBar from '../Items/PlayerBar';

export default function MusicScreen(props, route, navigation) {
  const [songs, setSongs] = useState([]);
  const [albums, setAlbums] = useState([]);

  const ref = fireStore.collection('songs');

  const ref1 = fireStore.collection('albums');

  useEffect(() => {
    (async () => {
      const querySnapshot = await ref1.get();
      const album = [];
      querySnapshot.forEach(doc => {
        album.push({
          id: doc.data().id,
          name: doc.data().name,
          url: doc.data().url,
        });
      });
      setAlbums(album);
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  // const onLogout = () => {
  //   firebaseApp
  //     .auth()
  //     .signOut()
  //     .then(function() {
  //       // Sign-out successful.
  //       console.log('logout complete !');
  //     })
  //     .catch(function(error) {
  //       // An error happened.
  //     });
  //   props.onLogin();
  // };

  return (
    <View style={styles.main}>
      <View style={{flex: 1}}>
        <ScrollView>
          {/*view header*/}
          <TouchableOpacity>
            <View style={styles.header}>
              <Text style={styles.textHeader}>Hosts now</Text>
            </View>
          </TouchableOpacity>
          {/*list item1*/}
          <View>
            <FlatList
              horizontal
              data={albums}
              renderItem={({item}) => (
                <MusicItem url={item.url} name={item.name} />
              )}
              keyExtractor={item => item.id}
            />
          </View>
          {/*item 2*/}
          <TouchableOpacity>
            <View style={styles.header}>
              <Text style={styles.textHeader}>Mood</Text>
            </View>
          </TouchableOpacity>
          {/*list item2*/}
          <View>
            <FlatList
              horizontal
              data={albums}
              renderItem={({item}) => (
                <MusicItem url={item.url} name={item.name} />
              )}
              keyExtractor={item => item.id}
            />
          </View>
          {/*item 2*/}
          <TouchableOpacity>
            <View style={styles.header}>
              <Text style={styles.textHeader}>Popular artis</Text>
            </View>
          </TouchableOpacity>
          {/*list item3*/}
          <View>
            <FlatList
              horizontal
              data={albums}
              renderItem={({item}) => (
                <MusicItem2 url={item.url} name={item.name} />
              )}
              keyExtractor={item => item.id}
            />
          </View>
        </ScrollView>
      </View>
      {/*<PlayerBar />*/}
    </View>
  );
}
const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: '#FAFAFA',
  },
  header: {
    // marginTop: 10,
    paddingLeft: 10,
  },
  textHeader: {
    fontSize: 25,
    color: 'orange',
  },
  list1: {
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10,
    width: 140,
  },
  textName: {
    fontSize: 15,
    fontWeight: 'bold',
  },
  textFollow: {
    fontSize: 10,
  },
  viewCenter: {
    alignItems: 'center',
  },
});
