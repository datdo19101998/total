import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  ActivityIndicator,
  ScrollView,
} from 'react-native';

import {BarIndicator} from 'react-native-indicators';

import {fireStore} from './firebaseConfig';

import Icon from 'react-native-vector-icons/FontAwesome';
import Shazam from 'react-native-vector-icons/Fontisto';
import Feather from 'react-native-vector-icons/Feather';

import Item from './../Items/SearchItem';
import List from './../Items/listRecents';
import ListAlbums from './../Items/ListAlbums';
import ProgressApp from './../Items/ProgressApp';
import PlayerApp from './../Items/Playerapp';

import algoliasearch from 'algoliasearch';

const client = algoliasearch('N700J98PZM', 'dafa8ceb40cb08184677a0df96127b1a');
const index = client.initIndex('songs');
export default function App(props) {
  const [text, setText] = useState([]);
  const [textSearch, setTextSearch] = useState('');
  const [songItems, setSongItems] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [loadingMore, setLoadingMore] = useState(true);
  const [onPlayerMusic, setOnPlayerMusic] = useState(false);
  const [dataPlayer, setDataPlayer] = useState([]);
  const [albums, setAlbums] = useState([]);
  const ref = fireStore.collection('recents');
  const ref2 = fireStore.collection('albums');
  const ref3 = fireStore.collection('fakeAlbums');

  useEffect(() => {
    (async () => {
      const querySnapshot = await ref.limit(9).get();
      const song = [];
      querySnapshot.forEach(doc => {
        song.push({
          id: doc.id,
          name: doc.data().text,
        });
      });
      setText(song);
      setIsLoading(true);
    })();
    (async () => {
      const querySnapshot = await ref2.limit(6).get();
      const albums = [];
      querySnapshot.forEach(doc => {
        albums.push({
          id: doc.id,
          name: doc.data().name,
          url: doc.data().url,
        });
      });
      // console.log(albums);
      setAlbums(albums);
      // console.log(albums);
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const searchFilterFunction = text => {
    if (text !== ' ') {
      setTextSearch(text);
    }
    index
      .search(textSearch)
      .then(({hits}) => {
        setSongItems(hits);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const addRecents = () => {
    if (textSearch !== '') {
      ref.add({
        text: textSearch,
      });
      // console.log(this.state.textSearch.length);
    } else {
      alert('Nhập để tìm kiếm');
    }
  };

  const printSong = () => {
    if (textSearch == '') {
      return (
        <ScrollView>
          <View>
            <View style={styles.container2}>
              <Text style={styles.header}>LỊCH SỬ</Text>
            </View>
            <View
              style={{
                flexWrap: 'wrap',
                flexDirection: 'row',
                paddingHorizontal: 10,
              }}>
              {text.map((item, index) => (
                <List
                  key={index}
                  name={item.name}
                  onAddRecent={addRecentTOState}
                />
              ))}
            </View>
            {text.length > 9 ? (
              <View />
            ) : (
              <View style={{alignItems: 'center', paddingHorizontal: 10}}>
                <TouchableOpacity onPress={loadMoreData}>
                  <Text style={{fontSize: 17, color: '#C2257F'}}>Xem Thêm</Text>
                </TouchableOpacity>
              </View>
            )}
            <View>
              <View style={styles.container2}>
                <Text style={styles.header}>CHỦ ĐỀ VÀ THỂ LOẠI</Text>
              </View>
              <View
                style={{
                  flexWrap: 'wrap',
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {albums.map((item, index) => (
                  <ListAlbums
                    key={index}
                    id={item.id}
                    name={item.name}
                    urlPath={item.url}
                  />
                ))}
              </View>
              <View style={{alignItems: 'center'}}>
                <TouchableOpacity
                  onPress={() => props.navigation.navigate('Home')}>
                  <Text
                    style={{
                      fontSize: 17,
                      color: '#C2257F',
                      marginHorizontal: 10,
                    }}>
                    Xem Thêm
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
      );
    } else {
      return (
        <FlatList
          data={songItems}
          renderItem={({item}) => (
            <Item
              // onDelete={this.onDelete}
              onPlayer={onPlayer}
              id={item.objectID}
              name={item.name}
              singer={item.singer}
              url={item.url}
              urlPlay={item.play}
            />
          )}
          keyExtractor={item => item.objectID}
        />
      );
    }
  };
  const loadMoreData = () => {
    ref.onSnapshot(snapshot => {
      const texts = [];
      snapshot.forEach(doc => {
        texts.push({
          id: doc.id,
          name: doc.data().text,
        });
      });
      setText(texts);
      // console.log(this.state.text);
    });
  };
  const onPlayer = data => {
    setDataPlayer(data);
    // console.log(data);
  };
  const addRecentTOState = name => {
    setTextSearch(name);
  };

  if (!isLoading) {
    return <BarIndicator count={5} color={'green'} />;
  } else {
    return (
      <View style={styles.main}>
        <View style={styles.container}>
          <View style={styles.left}>
            <TouchableOpacity>
              <Icon name="search" size={25} color="#dcdcdc" />
            </TouchableOpacity>
            <TextInput
              placeholder="Tìm kiếm ..!"
              style={styles.textInput}
              onChangeText={text => searchFilterFunction(text)}
              autoCorrect={false}
              value={textSearch}
            />
          </View>
          <View style={styles.right}>
            <View>
              <TouchableOpacity onPress={addRecents}>
                <Shazam name={'shazam'} size={50} color={'#059cff'} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={{flex: 1}}>{printSong()}</View>
        {dataPlayer.length !== 0 ? (
          <PlayerApp
            onPlayer={() =>
              props.navigation.navigate('Player', {
                name: dataPlayer[0].name,
                singer: dataPlayer[0].singer,
                url: dataPlayer[0].url,
                id: dataPlayer[0].id,
                play: dataPlayer[0].play,
              })
            }
            dataPlayer={dataPlayer}
          />
        ) : (
          <View />
        )}
      </View>
    );
  }
}

App.navigationOptions = () => {
  return {
    headerShown: false,
  };
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: 15,
    padding: 10,
    // paddingHorizontal: 10,
  },
  separator: {
    height: 1,
    width: '100%',
    backgroundColor: '#000',
  },
  left: {
    height: 50,
    backgroundColor: '#ffffff',
    flex: 1,
    borderRadius: 30,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingLeft: 30,
  },
  right: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingLeft: 7,
  },
  main: {
    backgroundColor: '#e5e5e5',
    height: '100%',
    flex: 1,
  },
  button: {
    backgroundColor: 'blue',
    height: '100%',
    width: 60,
    borderRadius: 30,
  },
  textInput: {
    marginLeft: 10,
    fontSize: 20,
    width: 250,
  },
  header: {
    fontSize: 25,
  },
  container2: {
    marginHorizontal: 15,
    // marginBottom: 15,
  },
  flatList: {
    flexWrap: 'wrap',
  },
  content: {
    alignItems: 'flex-start',
    flexDirection: 'column',
    flexWrap: 'wrap',
  },
  item: {
    padding: 10,
  },
  text: {
    fontSize: 15,
    color: 'black',
  },
  footer: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  loadMoreBtn: {
    padding: 10,
    backgroundColor: '#800000',
    borderRadius: 4,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    color: 'white',
    fontSize: 15,
    textAlign: 'center',
  },
});
