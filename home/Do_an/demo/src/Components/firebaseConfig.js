import firebase from 'react-native-firebase';

const config = {
  clientId:
    'client_id": "44995992215-nu3uh74s8p4k208i2usja26bq2oa7p8j.apps.googleusercontent.com',
  appId: '1:44995992215:android:9555f24d2c19cfeab167d2',
  apiKey: 'AIzaSyDBIhcMr4Z6JzdSxGp7v10M1KkhumS-MFU',
  databaseURL: 'https://rnlogin-b2b29.firebaseio.com',
  storageBucket: 'rnlogin-b2b29.appspot.com',
  messagingSenderId: '44995992215',
  projectId: 'rnlogin-b2b29',
};
const firebaseApp = firebase.initializeApp(config, 'demo');
export {firebaseApp};
const fireStore = firebaseApp.firestore();
export {fireStore};
