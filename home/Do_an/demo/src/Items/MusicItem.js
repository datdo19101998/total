import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  FlatList,
  ScrollView,
} from 'react-native';
import {fireStore} from '../Components/firebaseConfig';

export default function MusicScreen(props) {
  const DetailAlbums = name => {
    alert(name);
  };
  return (
    <TouchableOpacity onPress={() => DetailAlbums(props.name)}>
      <View style={styles.list1}>
        <Image
          style={{width: 160, height: 160, borderRadius: 10}}
          source={{
            uri: props.url,
          }}
        />
        <View style={styles.viewCenter}>
          <Text style={styles.textName}>{props.name}</Text>
          <Text style={styles.textFollow}>12033</Text>
          <Text style={styles.textFollow}>FOLLOWES</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}
const styles = StyleSheet.create({
  list1: {
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10,
    width: 160,
  },
  textName: {
    fontSize: 15,
    fontWeight: 'bold',
  },
  textFollow: {
    fontSize: 10,
  },
  viewCenter: {
    alignItems: 'center',
  },
});
