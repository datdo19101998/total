import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  FlatList,
  ScrollView,
} from 'react-native';
import {fireStore} from '../Components/firebaseConfig';

export default function MusicScreen(props) {
  return (
    <View style={styles.list1}>
      <Image
        style={{width: 80, height: 80, borderRadius: 10}}
        source={{
          uri: props.url,
        }}
      />
    </View>
  );
}
const styles = StyleSheet.create({
  list1: {
    marginTop: 5,
    marginLeft: 10,
    marginRight: 5,
    width: 80,
  },
  textName: {
    fontSize: 15,
    fontWeight: 'bold',
  },
  textFollow: {
    fontSize: 10,
  },
  viewCenter: {
    alignItems: 'center',
  },
});
