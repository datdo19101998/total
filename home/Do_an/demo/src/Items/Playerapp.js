import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Text,
  ProgressBarAndroid,
  TouchableOpacity,
} from 'react-native';
import PlayIcon from 'react-native-vector-icons/AntDesign';

export default function PlayerApp(props) {
  const [pausePlay, setPausePlay] = useState(true);
  const PauseAndPlay = () => {
    setPausePlay(!pausePlay);
  };
  return (
    <>
      <View>
        <ProgressBarAndroid
          styleAttr="Horizontal"
          indeterminate={false}
          progress={0.8}
          color={'#ff6d26'}
        />
      </View>
      <View style={{justifyContent: 'space-between', flexDirection: 'row'}}>
        <View style={styles.container5} />
        <View style={styles.container4}>
          <TouchableOpacity onPress={props.onPlayer}>
            <Text
              numberOfLines={1}
              ellipsizeMode="tail"
              // onPress={onPlay}
              style={{fontSize: 17, marginTop: 10}}>
              {props.dataPlayer[0].name}
            </Text>
          </TouchableOpacity>
          <Text style={{fontSize: 15, marginBottom: 10, color: '#ff6d26'}}>
            {props.dataPlayer[0].singer}
          </Text>
        </View>
        <View style={styles.container5}>
          <TouchableOpacity onPress={PauseAndPlay}>
            {pausePlay ? (
              <PlayIcon name="pausecircleo" size={45} color="#ff6d26" />
            ) : (
              <PlayIcon name="playcircleo" size={45} color="#ff6d26" />
            )}
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container4: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    width: '70%',
  },
  container5: {
    width: '15%',
    backgroundColor: 'white',
    justifyContent: 'center',
  },
});
