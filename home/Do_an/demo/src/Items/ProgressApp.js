import React, {Component} from 'react';
import {View, StyleSheet, ProgressBarAndroid} from 'react-native';

export default function Progress(props) {
  return (
    <View>
      <ProgressBarAndroid
        styleAttr="Horizontal"
        indeterminate={false}
        progress={0.8}
        color={'#ff6d26'}
      />
    </View>
  );
}
