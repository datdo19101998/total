import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';

export default function List(props) {
  const name = props.name;
  const onAddRecent = name => {
    props.onAddRecent(name);
  };
  return (
    <TouchableOpacity onPress={() => onAddRecent(name)}>
      <Text style={styles.text}>{name}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  text: {
    fontSize: 20,
    margin: 5,
    borderWidth: 1,
    borderColor: '#C2257F',
    paddingHorizontal: 10,
    borderRadius: 20,
    color: '#C2257F',
  },
});
