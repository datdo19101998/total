import React, {useState, useEffect} from 'react';
import {View, Text, Button} from 'react-native';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import Feather from 'react-native-vector-icons/Feather';

import MusicScreen from './MusicScreen';
import SearchScreen from './SearchScreen';
import ProfileScreen from './ProfileScreen';

const Tab = createMaterialBottomTabNavigator();

export default function LoginScreen({navigation}) {
  return (
    <Tab.Navigator
      initialRouteName="Music"
      activeColor="orange"
      inactiveColor="#3e2465"
      barStyle={{backgroundColor: 'white'}}>
      <Tab.Screen
        options={{
          tabBarLabel: 'Trang chủ',
          tabBarIcon: ({focused}) => (
            <Feather
              size={25}
              name={'headphones'}
              color={focused ? 'orange' : 'gray'}
            />
          ),
        }}
        name="Music"
        component={MusicScreen}
      />
      <Tab.Screen
        options={{
          tabBarLabel: 'Tìm kiếm',
          tabBarIcon: ({focused}) => (
            <Feather
              size={25}
              name={'search'}
              color={focused ? 'orange' : 'gray'}
            />
          ),
        }}
        name="Search"
        component={SearchScreen}
      />
      <Tab.Screen
        options={{
          tabBarLabel: 'Tài khoản',
          tabBarIcon: ({focused}) => (
            <Feather
              size={25}
              name={'user'}
              color={focused ? 'orange' : 'gray'}
            />
          ),
        }}
        name="Profile"
        component={ProfileScreen}
      />
    </Tab.Navigator>
  );
}
