import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Alert,
  ImageBackground,
  Image,
  TouchableOpacity,
} from 'react-native';

import {firebaseApp} from './../Components/firebaseConfig';

import {Input, Button} from 'react-native-elements';

import Icon from 'react-native-vector-icons/Fontisto';

export default function LoginScreen({navigation}) {
  const [email, setEmail] = useState('');
  const [passWord, setPassWord] = useState('');

  const onLogin = () => {
    if (email !== '' && passWord !== '') {
      firebaseApp
        .auth()
        .signInWithEmailAndPassword(email, passWord)
        .then(() => {
          Alert.alert(
            'Thông báo ',
            'Đăng nhập thành công ' + email,
            [
              {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              {
                text: 'OK',
                onPress: () => navigation.navigate('App'),
              },
            ],
            {cancelable: false},
          );
          setEmail('');
          setPassWord('');
        })
        .catch(function(error) {
          Alert.alert(
            'thông  báo',
            'Sai tên tài  khoản hoặc mật khẩu !',
            [
              {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              {
                text: 'OK',
              },
            ],
            {cancelable: false},
          );
        });
    } else {
      alert('Tên tài khoản hoặc mật khẩu không hợp lệ !');
    }
  };

  return (
    <ImageBackground
      source={require('./../../public/images/hinh-nen-dai-mau-tim-do-dep-cho-dien-thoai.jpg')}
      style={{width: '100%', height: '100%'}}>
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          // alignItems: 'center',
        }}>
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <Text style={styles.header}>Đăng nhập </Text>
        </View>
        <View style={{marginHorizontal: 20}}>
          <Input
            placeholder="email "
            leftIcon={<Icon name="email" size={24} color="black" />}
            onChangeText={mail => setEmail(mail)}
            value={email}
          />
          <Input
            placeholder="Password "
            textContentType={'password'}
            leftIcon={<Icon name="key" size={24} color="black" />}
            onChangeText={pass => setPassWord(pass)}
            value={passWord}
            secureTextEntry={true}
          />
        </View>
        <View style={{justifyContent: 'space-around', flexDirection: 'row'}}>
          <View style={{marginTop: 10, width: 100}}>
            <Button onPress={onLogin} title="Login" />
          </View>
          <View style={{marginTop: 10, width: 100}}>
            <Button
              title="Register"
              onPress={() => navigation.navigate('Register')}
            />
          </View>
        </View>
        <TouchableOpacity onPress={() => navigation.navigate('Forget')}>
          <View style={styles.forget}>
            <Text style={styles.textMore}>Quên mật khẩu ?</Text>
          </View>
        </TouchableOpacity>
        {/*<View*/}
        {/*  style={{*/}
        {/*    borderWidth: 1,*/}
        {/*    borderBottomColor: 'gray',*/}
        {/*    marginHorizontal: 100,*/}
        {/*    marginTop: 20,*/}
        {/*  }}*/}
        {/*/>*/}
      </View>
    </ImageBackground>
  );
}
const styles = StyleSheet.create({
  header: {
    fontSize: 35,
    color: 'white',
  },
  options: {
    alignItems: 'center',
    marginTop: 10,
  },
  textMore: {
    fontSize: 15,
    color: 'white',
  },
  forget: {
    marginTop: 10,
    alignItems: 'center',
  },
});
