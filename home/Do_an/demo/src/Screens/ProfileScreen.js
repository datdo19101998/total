import React, {useState, useEffect} from 'react';
import {
  Button,
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  FlatList,
  ScrollView,
} from 'react-native';

import {ListItem} from 'react-native-elements';
import {firebaseApp, fireStore} from '../Components/firebaseConfig';

import Icon from 'react-native-vector-icons/Octicons';
import Star from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';

const list = [
  {
    title: 'Bài hát ',
    icon: 'flight-takeoff',
  },
  {
    title: 'Download',
    icon: 'flight-takeoff',
  },
];
export default function ProfileScreen(props) {
  const [id, setId] = useState('');
  const [Profiles, setPro] = useState([]);
  const [Likes, setLikes] = useState([]);
  const [mySong, setSong] = useState([]);

  useEffect(() => {
    const user = firebaseApp.auth().currentUser;
    if (user) {
      // User is signed in.
      setId(user.uid);
      // setEmail(user.email);
    } else {
      // No user is signed in.
    }

    const clLikes = fireStore
      .collection('users')
      .doc(id)
      .collection('likes');
    const clSong = fireStore
      .collection('users')
      .doc(id)
      .collection('mySong');
    const clProfile = fireStore.collection('users');

    clLikes.get().then(function(querySnapshot) {
      const likes = [];
      querySnapshot.forEach(function(doc) {
        // doc.data() is never undefined for query doc snapshots
        // console.log(doc.id, ' like => ', doc.data());
        likes.push(doc.data());
      });
      setSong(likes);
    });
    clSong.get().then(function(querySnapshot) {
      const songs = [];
      querySnapshot.forEach(function(doc) {
        // doc.data() is never undefined for query doc snapshots
        // console.log(doc.id, 'songs => ', doc.data());
        songs.push(doc.data());
      });
      setLikes(songs);
    });
    clProfile.get().then(function(querySnapshot) {
      const profiles = [];
      querySnapshot.forEach(function(doc) {
        // doc.data() is never undefined for query doc snapshots
        // console.log(doc.id, 'profile => ', doc.data());
        profiles.push(doc.data());
      });
      setPro(profiles);
    });
  }, [Profiles, id]);

  return (
    <View style={{flex: 1}}>
      <View style={{flex: 1}}>
        <ScrollView>
          <View style={{alignItems: 'center'}}>
            {/*banner*/}
            <View style={Style.banner}>
              <View>
                <Text style={Style.text1}>SoundCloud Premium</Text>
                <Text style={Style.text2}>
                  Remove boring advs, create infinite playlists and so much
                  more...
                </Text>
              </View>
              <View>
                <Icon
                  name={'x'}
                  size={16}
                  color={'white'}
                  style={Style.icon1}
                />
                <Feather
                  name={'loader'}
                  size={20}
                  color={'white'}
                  style={Style.icon2}
                />
                <Star
                  name={'star'}
                  size={40}
                  color={'white'}
                  style={Style.icon3}
                />
                <Feather
                  name={'loader'}
                  size={15}
                  color={'white'}
                  style={Style.icon4}
                />
              </View>
            </View>
            {/*end banner*/}
          </View>
          {/*mid*/}
          <View style={Style.mid}>
            <View style={{flex: 1, marginTop: 15, marginBottom: 15}}>
              <Text
                numberOfLines={1}
                ellipsizeMode={'tail'}
                style={{fontSize: 25}}>
                {Profiles.email}
              </Text>
              <Text>Edit profile</Text>
            </View>
            <View style={{marginTop: 15, marginBottom: 15}}>
              <Image
                style={{width: 50, height: 50, borderRadius: 25}}
                source={{
                  uri:
                    'https://facebook.github.io/react-native/img/tiny_logo.png',
                }}
              />
            </View>
          </View>
          {/*end mid*/}
          <View style={{flex: 1, backgroundColor: '#FAFAFA'}}>
            <View>
              {list.map((item, i) => (
                <TouchableOpacity key={i} onPress={() => alert(item.title)}>
                  <ListItem
                    containerStyle={{backgroundColor: '#FAFAFA'}}
                    title={item.title}
                    // leftIcon={{name: item.icon}}
                    bottomDivider
                    chevron
                  />
                </TouchableOpacity>
              ))}
            </View>
            <View>
              <View style={{paddingLeft: 10}}>
                <Text style={Style.text3}>Playlist</Text>
              </View>
              {/*<PlayList albums={albums} />*/}
            </View>
          </View>
          {/*end playlist*/}
          <View style={Style.LogOut}>
            <Text style={Style.textLogOut}>Đăng xuất</Text>
          </View>
        </ScrollView>
      </View>
      {/*<View>*/}
      {/*  <PlayerBar />*/}
      {/*</View>*/}
    </View>
  );
}
const Style = StyleSheet.create({
  banner: {
    backgroundColor: '#ff732f',
    height: 180,
    width: 380,
    flexDirection: 'row',
    borderRadius: 13,
    marginTop: 20,
  },
  container: {
    marginLeft: 15,
    marginTop: 15,
  },
  view1: {
    flexDirection: 'row',
    marginRight: 10,
  },
  view2: {
    flexDirection: 'column',
    marginRight: 10,
  },
  view3: {
    flexDirection: 'row',
    marginTop: 20,
  },
  view4: {
    alignSelf: 'flex-end',
    marginLeft: 120,
  },
  text1: {
    color: 'white',
    fontSize: 24,
    width: 200,
    marginTop: 12,
    marginLeft: 12,
    fontFamily: 'sans-serif',
  },
  text2: {
    color: 'white',
    fontSize: 16,
    width: 220,
    marginLeft: 12,
    fontFamily: 'san-serif',
  },
  text3: {
    fontSize: 28,
    fontFamily: 'sans-serif-medium',
  },
  text4: {
    color: '#696969',
  },
  icon1: {
    alignSelf: 'flex-end',
    marginLeft: 125,
    marginTop: 10,
  },
  icon2: {
    alignSelf: 'flex-end',
    marginTop: 15,
    marginRight: 60,
    opacity: 0.6,
  },
  icon3: {
    alignSelf: 'flex-end',
    marginRight: 20,
  },
  icon4: {
    alignSelf: 'flex-end',
    marginTop: 15,
    marginRight: 10,
    opacity: 0.4,
  },
  img: {
    height: 60,
    width: 60,
    borderRadius: 30,
  },
  header: {
    fontSize: 26,
    marginLeft: 20,
    paddingTop: 5,
    paddingBottom: 20,
    fontFamily: 'sans-serif-medium',
  },
  mid: {
    backgroundColor: '#FAFAFA',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 20,
  },
  LogOut: {
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FAFAFA',
  },
  textLogOut: {
    color: 'orange',
    fontSize: 20,
    fontWeight: 'bold',
  },
});
