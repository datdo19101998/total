import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Alert,
  ImageBackground,
  Image,
} from 'react-native';

import {Input, Button} from 'react-native-elements';

import Icon from 'react-native-vector-icons/Fontisto';

import {firebaseApp} from './../Components/firebaseConfig';

export default function RegisterScreen(props) {
  const [email, setEmail] = useState('');
  const [passWord, setPassWord] = useState('');

  const DangKy = () => {
    firebaseApp
      .auth()
      .createUserWithEmailAndPassword(email, passWord)
      .then(() => {
        Alert.alert(
          'Alert Title',
          'Đăng ký thành công !' + email,
          [
            {
              text: 'Cancel',
              onPress: () => console.log('Cancel Pressed'),
              style: 'cancel',
            },
            {
              text: 'OK',
              onPress: () => props.navigation.navigate('Login'),
            },
          ],
          {cancelable: false},
        );
        setEmail('');
        setPassWord('');
      })
      .catch(function(error) {
        Alert.alert(
          'Alert Title',
          'Đăng ký thất bại , vui lòng chọn tên email khác !',
          [
            {
              text: 'Cancel',
              onPress: () => console.log('Cancel Pressed'),
              style: 'cancel',
            },
            {text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          {cancelable: false},
        );
      });
  };

  return (
    <ImageBackground
      source={require('./../../public/images/hinh-nen-dai-mau-tim-do-dep-cho-dien-thoai.jpg')}
      style={{width: '100%', height: '100%'}}>
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          // alignItems: 'center',
        }}>
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <Text style={styles.header}>Đăng ký </Text>
        </View>
        <View style={{marginHorizontal: 20}}>
          <Input
            placeholder="email name"
            leftIcon={<Icon name="email" size={24} color="black" />}
            onChangeText={email => setEmail(email)}
            value={email}
            autoCompleteType={'email'}
          />
          <Input
            placeholder="Password "
            textContentType={'password'}
            leftIcon={<Icon name="key" size={24} color="black" />}
            onChangeText={passWord => setPassWord(passWord)}
            value={passWord}
            secureTextEntry={true}
          />
        </View>
        <View style={{justifyContent: 'space-around', flexDirection: 'row'}}>
          <View style={{marginTop: 10, width: 100}}>
            <Button title="Register" onPress={DangKy} />
          </View>
        </View>
      </View>
    </ImageBackground>
  );
}
const styles = StyleSheet.create({
  header: {
    fontSize: 35,
    color: 'white',
  },
});
