import React from 'react';
import LoginGoogle from './src/LoginGoogle';
import LoginFacebook from './src/LoginFacebook';
import App from './src/App';

const MaxApp = () => {
  return <App />;
};
export default MaxApp;
