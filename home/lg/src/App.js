import React from 'react';
import LoginGoogle from './LoginGoogle';
import LoginFacebook from './LoginFacebook';

const App = () => {
  return (
    <>
      <LoginGoogle />
    </>
  );
};
export default App;
